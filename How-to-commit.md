# Design by Committing, not by Committee!

In this document, I'll run through the core technical process involved in a [_Cut, Copy & Paste_](README.md) workshop: creating a file, copying and committing it to the repository, and enabling somebody else to build upon that work to make something new.

----

![blank page in a typewriter](Images/Demo\ sketches/01_typewriter.png)
OK, so I'm all prepared to create. 

I'm in front of the typewriter, I'm ready to amaze the literary world with my astonishing debut work. I can feel the muse coming on now...

!['hello world' is typed](Images/Demo\ sketches/02_helloworld.png)
There it is, folks. A work of genius :)

![paper is removed from the typewriter](Images/Demo\ sketches/03_remove-page.png)
I've got to tell the world about my opus.

![time to commit!](Images/Demo\ sketches/04_time-to-commit.png)
let's commit this to our pool of commons, so that others can take it further.

![the commit table](Images/Demo\ sketches/05_commit-table.png)
This is the commit table, where we are going to mark and log the work so others can find it.

![the log page is blank, my work is still as it was](Images/Demo\ sketches/06_commit-table.png)
I'm going to make a new entry in the [log](https://gitlab.com/cameralibre/cut-copy-and-paste/blob/master/Commit-Log/Commit-Log_A3_Portrait.pdf).

![image of a page numbering machine with the name 'The Commiter'](Images/Demo\ sketches/07A_the-commiter.png)

We're going to use a special tool, an incrementing page stamp, to assign a unique number to the work that I have just done.


![the work is stamped with commit number ''1734'](Images/Demo\ sketches/07_commit-no-1.png)
I stamp once on my work...

![under 'Commit No.', the log is stamped with the same number, '1734'](Images/Demo\ sketches/08_commit-no-2.png)
...and once on the log. The incrementing stamp will automatically move on the the next number after being stamped twice, so nobody else can accidentally stamp their work with the number 1734. That number refers only to the work done when I typed 'hello world' on that piece of paper.

![the license symbol for CC-BY is added to the log](Images/Demo\ sketches/09_commit-cc-by.png)
I choose the CC-BY license and stamp it in the log alongside the commit. Participants can choose between the [Creative Commons Attribution](https://creativecommons.org/licenses/by/4.0/) or [Creative Commons Attribution-ShareAlike](https://creativecommons.org/licenses/by-sa/4.0/) licenses, or apply the [Creative Commons Zero](https://creativecommons.org/share-your-work/public-domain/cc0) copyright waiver to their work. 

The reason to have [these stamps](CC-Rubber-Stamps/README.md) in the workshop (rather than use some kind of blanket agreement which applies the same license to all commits) is for participants to understand that if they _do not_ apply a license to their work, it is All Rights Reserved and therefore impossible for others to use it. 

In order to make their creative work 'fertile', they must apply an open source license - which they can do by stamping the commit log with one of these stamps, alongside their name stamp. 

![The name 'SAM' is stamped under the 'author' column](Images/Demo\ sketches/10_commit-author.png)
When participants join the workshop, they can create their own name badge from a collection of alphabet stamps. I'll stamp my own name under the 'author' column.

![The comment 'typed hello world' is added to teh comment column](Images/Demo\ sketches/11_commit-comment.png)

This is an original work, so I will leave the 'Builds upon' column blank. I'll leave a very brief description of what I did in the Comment column, so that the history of this work can be tracked later.

![the log page is gone, only the original work remains](Images/Demo\ sketches/12_copy-01.png)
That's everything that we need to include in the log for now, so we'll move on the the next stage.

![a photocopier appears next to the page](Images/Demo\ sketches/12_copy-02.png)
I'll head over to the photocopier.

![the photocopier opens](Images/Demo\ sketches/12_copy-03.png)
open wide...

![the page is being photocopied](Images/Demo\ sketches/12_copy-04.png)
I'll photocopy my page.

![a B+W copy now stands next to the original](Images/Demo\ sketches/12_copy-05.png)
ta-da! now we have the original, and a copy.

![an arrow points from the original to an expanding file, and from the copy to a 'help wanted' board](Images/Demo\ sketches/13_file.png)
I will file the original in the repository, and add my copy to the 'help wanted' noticeboard. Let's think about how others might be able to take this further.

![a note saying 'how about an illustration?' is added to the copy on noticeboard](Images/Demo\ sketches/14_suggestion.png)
Let's see if somebody wants to work on the visual appeal of this work.

[Judith](http://judithcarnaby.com/) sees the note - she's a talented illustrator and is helping with the _Cut, Copy & Paste_ workshops. 

![an illustration of the Earth, waving is added to the lower half of the work](Images/Demo\ sketches/15_world-illustration.png)
Judith adds her own take on the work. Note: it is important to mention (for Judith's professional reputation) that this sloppy drawing is in fact by Sam, not by her :) 


![the commit number '1734' has now been covered by a sticker](Images/Demo\ sketches/16_sticker.png)
She heads to the commit table and covers the old commit number with a sticker...

![the new commit number '1735' is added to the work](Images/Demo\ sketches/17_new-number.png)
and stamps a new, unique commit number on the work. 

![The log shows the old entry for 1734, and Judith's new entry, 1735.](Images/Demo\ sketches/18_new-commit.png)
Judith adds this new commit to the log, with her name and license.

![the commit number '1734' is also added to the log under 'Builds Upon'](Images/Demo\ sketches/19_builds-upon-1734.png)
As this work builds upon an earlier commit, Judith needs to make a note of it in the log. Under 'Builds upon' she adds the commit number of the work she used - in this case, '1734' to refer to the commit number of my work. 


![the comment 'DREW THE EARTH WAVING' is also added to the log](Images/Demo\ sketches/19_comment.png)
She also adds a description in the comment section so that people know what has changed. 


![the copy of judith's commit is visible next to the original](Images/Demo\ sketches/20_the-cycle-continues.png)
She can then file her original version of 1735 in the repository and post the copy of 1735 on the noticeboard.

In case somebody decides they want to use a different illustration, they can go back to an earlier version of 'hello world': they can make another copy of 1734 from the repository, and work on that copy. 

----

**Note:** This is the technical nuts-and-bolts of the commit process in Cut, Copy & Paste workshops. It is _necessary_, but it's hardly the most interesting part. 

(Head to the [README](README.md) to find out more about the project in general)

What _is_ important is what this process enables - as the pool of commons grows, and different works are taken in different directions, the works can evolve down different creative paths at the same time. They can be translated and remixed as much as you like, while at the same time Judith or I (or anybody else) could maintain different forks with our own particular takes on the work. Near the end of a workshop, participants can then create a collection of works by different people, from any stage in those works' development. The works can be adapted further or left as they are, and each participant can collate them into their own unique zine. 

Thus, each participant is able to achieve more, through building upon the input and creativity of the whole group. At the same time, this is not design-by-committee or brainstorming. Bland consensus is not required. 
The subjectivity and creativity of each individual can be expressed by maintaining **their own selection** of that commons, and making changes **as they see fit**.
