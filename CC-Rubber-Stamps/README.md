# Creative Commons stamp designs
These files can be used to have custom rubber stamps made - many print services or stamp specialists allow you to upload a file which they will turn into a rubber stamp for you (for company logos etc). These are small stamps which I have ordered in sizes of 30×10mm or 20×10mm (it cost me €15 total for the 3 stamps). Because I am interested in building a useful commons, I chose only free culture licenses:

![Creative Commons Attribution license](CC-BY_stamp_20x10mm.png)

[Creative Commons Attribution](https://creativecommons.org/licenses/by/4.0/) - 20mm×10mm

![Creative Commons Attribution ShareAlike license](CC-BY-SA-stamp_30x10mm.png)

[Creative Commons Attribution-ShareAlike](https://creativecommons.org/licenses/by-sa/4.0/) - 30mm×10mm

![Creative Commons Zero copyright waiver](CC0stamp_30x10mm.png)

[Creative Commons Zero](https://creativecommons.org/share-your-work/public-domain/cc0) (essentially 'No Rights Reserved', a public domain dedication) - 30mm×10mm

The reason to have these stamps in the workshop is for participants to understand that if they do not apply a license to their work, it is All Rights Reserved and therefore impossible for others to use it. 
In order to make their creative work 'fertile', they need to apply an open source license - which they can do by stamping the commit log with one of these stamps, alongside their name stamp. 
The use of rubber stamps rather than manually writing a license note represents the quick, easy and legally safe practice of using standard licenses in the open source world - you don't need to write your own license, and you should not add any extra clauses to standard licenses: just choose a proven, trusted and open source license, as-is, without any 'no-derivatives' or '[non-commercial](http://community.oscedays.org/t/why-are-non-commercial-licenses-not-open-source/569)' clauses.