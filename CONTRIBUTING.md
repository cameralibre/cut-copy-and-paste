I would love to have more people involved in developing and running Cut, Copy & Paste workshops.

### Can I use this?
Of course - if you would like to run a version of these workshops, go ahead - you don't need my permission! But I'm very happy to help you adapt them, or to look for solutions to any problems you may come across. I'd love to hear what you do with it!

I will be based in Aotearoa New Zealand from 2018, so I'd love other people to pick up the project and run workshops in places which are harder for me to get to (so... pretty much anywhere).

If you publish a new version, you need to attribute me and use the same license that I have used. (Everything in this repository (unless otherwise indicated) is licensed under [Creative Commons Attribution-Sharealike](https://gitlab.com/cameralibre/cut-copy-and-paste/blob/master/Posters/CC-BY-SA-WTF.pdf).)

### I want take part in a workshop!

If you would like me (or someone else) to run a workshop for you or your organisation, please get in touch, I can help you to find the right kind of workshop and the best person/people to facilitate it.

### I have an idea! / Check out this resource! / [You are WRONG on the internet](https://www.xkcd.com/386/).
Have you seen other interesting approaches to teaching open source to artists? 

How would you communicate the open source approach in a fun, lo-tech way? 

Can you solve any of these [Issues](https://gitlab.com/cameralibre/cut-copy-and-paste/issues)?

If you have any suggestions or improvements please get in touch:
 
- [my website](http://www.cameralibre.cc/)  
- [twitter](https://twitter.com/cameralibre)  
- on the [Open Source Design](https://discourse.opensourcedesign.net/t/teaching-open-source-collaboration-to-designers-without-digital-tools/289) forum
- or open an [issue](https://docs.gitlab.com/ee/user/project/issues/index.html) here on Gitlab. If you go to this project's [Issues](https://gitlab.com/cameralibre/cut-copy-and-paste/issues) page, you will see the option to simply email an issue. Follow the instructions and if you need any help with Git or Gitlab, just ask - a lot of it is over my head too, we can learn together :)