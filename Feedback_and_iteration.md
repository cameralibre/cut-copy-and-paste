# BLOG

## Making MozFest zines using (analog!) open source collaboration


**Workshop:** [Use (analog!) Open Source collaboration to make your own MozFest zine)](https://github.com/MozillaFoundation/mozfest-program-2017/issues/472)

**Event:** [Mozilla Festival 2017](https://mozillafestival.org/)

----

On the 28th & 29th of October, [Judith Carnaby](http://judithcarnaby.com/) and I ran a Cut, Copy & Paste workshop as an ongoing interactive installation in the Open Innovation space at MozFest. 
The range of participants was very diverse- from children to middle-age, from multiple different cultural backgrounds, but also diverse in terms of interests and experience with open source. Some were developers who use git every day, others had never heard of open source before.

A blog post is in development and will be published this week.

Overall, running our workshop at the event was a positive experience, for us and our participants, but the 'drop-in' nature of the workshop was exhausting for us as facilitators, and I would not choose to run it in this format again.

### What worked?

- we got all the equipment and posters organised in time to run a full workshop
- passers by were intrigued by the typewriter and the moveable type name stamps, which drew them in to the workshop.
- we had a handful of repeat visitors who loved the workshop, and kept returning to remix work
- repeat visitors could be called on to teach others
- we had a number of divergent evolutions, which is what I was looking for: works that could evolve in different directions in parallel.
- There was enough material for Judith and I to make a couple of coherent zines from the commons we built together.
- Changes to the commit process were successful. Standardized [license stamps](CC-Rubber-Stamps) and [dropping the filename requirement](https://discourse.opensourcedesign.net/t/convincing-designers-to-embrace-os-methods/312/7) both helped speed up and simplify the process (#9 , #10 )
- no bottlenecks at the commit table (#9 )
- this workshop had many more visual / illustrated contributions than OSD Summit (#12 )

### What didn't?

- **we didn't prepare and display a useful Public Domain repository** for inclusion in the remix process (#4 )
- **Two days is too long!** (#7 )
- **'Drop-in' installations of the workshop are inefficient** (#15 ) - Many visitors to the installation at MozFest only made one contribution, and many others only want to hear the explanation and then do not contribute. This makes for an exhausting experience for the facilitators - it required a ratio of about 3 or 4 explanations of the process for each single commit.. 
- **Participants seldom used the posters**, preferring to ask the facilitators how it worked. (#8, #10 )
- **Participants did not have enough time to make zines** / there were too many distractions - only Judith and I were able to spend time on Sunday afternoon curating & assembling zines from the works created (#11 )

### Potential Solutions:
Through the experience of the last two workshops, I have come to the conclusion that a focused 2-hour workshop with 5-20 participants would be ideal, and I will aim to make happen for the next iteration.