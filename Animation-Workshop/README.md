I'm making a version of Cut, Copy & Paste designed for collaborative animation! 


Some development has been taking place with input from community members on [ssb](https://www.scuttlebutt.nz/):

[Thread](%WvjH8P4vdpGqMjCy5MdVex7KEZWuJ8LAwveHo28OU3Y=.sha256)

You'll need a scuttlebutt client to open that link and participate in the discussion, but do [say hi to me](@UhORGzAhEE3gqy/pH5vK+EgbpZfnyYvdI46TVBJH6Mw=.ed25519) when you arrive in the scuttleverse :)


Further info from the Cut, Copy & Paste [Roadmap](../ROADMAP.md):

### ✏ ANIMATION WORKSHOP 🎞
Develop a Cut, Copy & Paste workshop for making simple 2D black & white animations using the photocopier. 
Each A4 page is cut into 12× frames, making a 1-second animation @ 12fps. 

![](animation-layout.svg)

#### Example animations (due March 29th)
Make a series of example animations to test out the process and ultimately communicate the workshop itself. I plan to present this workshop as a proposal to the Wellington Motion Graphics meetup, which meets on or around the 29th of March.

#### Develop 'Gameplay'
- Work out how co-creation of animations should occur
- come up with games/activities for getting participants started
- come up with methods to avoid bottlenecks

#### Bash script 1: scanned A4 -> video
_(see #17 for current status of development)_
Write a script using ffmpeg which cuts a 600dpi scan of an animation into a 12-frame image sequence, and makes a video file from it (necessary to allow participants to immediately review their work). 
User input should allow for: 

**0:** a 1-second video, just the 12 frames of animation.

**1:** a 2-second video, comprising a 1-sec preroll of the first frame, then the 12-frame animation.

**2:** a 2-second video, comprising the 12-frame animation, then a 1-sec hold of the last frame, 

**3:** a 3-second video, comprising a 1-sec preroll of the first frame, then the 12-frame animation, then a 1-sec hold of the last frame.

The script should then play the video. 

#### Bash script 2: Backgrounds
_(see #17 for current status of development)_
Write a script using imagemagick which: 

- takes a scanned A4 page containing a background illustration 

- crops it to the correct aspect ratio

- scales down & arranges that image 12x as 12 frames on a new A4 page

- prints the image

