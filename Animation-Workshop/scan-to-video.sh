#!/bin/bash
width=1754
height=1653

magick input.png -crop "${width}x${height}" temp.png

for n in $(seq 0 11); do
  N=$(printf %02d $n)
  mv temp-${n}.png output-${N}.png
done

rm temp-{12..15}.png

ffmpeg -r 12 -i output-%02d.png -c:v prores_ks -profile:v 3 -qscale:v 10 -vf "fps=12,format=yuv422p10le" -s 1754x1653 -r 12 outputVideo.mov

rm output-{00..11}.png

vlc --repeat outputVideo.mov
