## Standard equipment:

- **A4 paper**
- **sticky notes** (post-its)
- **Sharpie pen** or similar, for writing issues visibly on sticky notes
- **black ink pen**(s) for drawing
- **ballpoint pens** for writing in the commit log
- **scissors**
- **glue stick**(s)
- **ruler**(s)
- **white stickers** for covering old commit numbers 
- **stapler** for putting the zines together (don't forget the **staples** like we did...)
- **inkpad**(s) for rubber stamps
- an **expanding file** to function as a repository.
- a **paper cutter**, or **utility knife** and **cutting board** for trimming the zines
- 3 tables: a **Create table**, a **Commit Table**, and a **Remix Table**
![](Images/README-Images/triangle-layout.png)



## Specialised/harder-to-find equipment:

- a **Commit Log** printed on either A3 or A4 sheets - adapt/print the files from the [Commit Log](Commit-Log) folder.

- **Instructional posters** - these can be referred to by participants to clarify steps in the process, or explain, for example, why open source licenses are necessary. Adapt and print using files from the [Instructional Posters](Print/Instructional-Posters) folder.

- **Creative Commons rubber stamps** - these need to be custom made, but it's not expensive, should only take a few days to arrive, and you can use the design files in the [CC Rubber Stamps](CC-Rubber-Stamps) folder. It is possible for participants to simply write in the license name with a pen, but one of the symbolic aspects that the stamps help communicate is that CC licenses are well-understood and functional **standards** which should not be 'customised' with personal clauses and additions. Only using stamps also prevents copyright nerds from overcomplicating things by writing in _WTFPL_ and _AGPL version 1_...

- **'The Committer'**, an [automatic page-numbering stamp](https://en.wikipedia.org/wiki/Bates_numbering). The only requirement is that it needs to have a setting for duplicate numbering, i.e. it will print the same number **twice** before incrementing to the next number. I bought a used [Reiner B6](http://www.reiner.de/index.php?B6_Handstempel_Numerieren_mit_und_ohne_Text_en), which cost €20 on eBay. If you can't get your hands on one of these, it _is_ possible for participants to write their own commit numbers by hand, but it can make legibility a little more difficult, and it's not nearly as much fun as using the big stamp! I also like the 'automated' aspect of using the Committer because it's more like digital version control - ```git``` will automatically assign a unique commit number to your commit, you don't need to write one in yourself.

- **Photocopier** or **multi-function printer** (MFP). This can be a small desktop printer/copier, as it doesn't need to do complex or high-speed printing, and only needs to produce black & white A4. The speed of the copier does become an issue if you have a lot of participants (>15). I would recommend trying to borrow a copier, as hiring a copier can be very expensive. In London it cost £180 to hire an A4 MFP for two days (including delivery), which was the cheapest quote I could find.

## Optional:

- **Letter stamps** for making the authors' names. These are not _strictly_ necessary, of course the participants could write their names with a pen. But name stamps do make the names very legible, which is important for attribution, and it can be a nice metaphor for github/gitlab handles, the unique online identity which refers to a person... and people love to make their own name stamps. I ordered 20× alphabet sets from a seller on [AliExpress](https://www.aliexpress.com/item/English-Alphabet-Capital-Letter-Rubber-Stamp-Free-Combination-DIY-Seal-Pattern-Stamps/32717795321.html), though I adjusted the numbers of letters that I brought to the workshop according to [letter frequency](https://en.wikipedia.org/wiki/Letter_frequency) in major European languages - in a London-based workshop, there isn't much use for all 20× _Z_s, _Q_s or _X_s, though there may be in other areas/cultures.
Alternative options include using individual [animal stamps](#20) or similar - these stamps are low-cost and much more widely available than letter stamps.

- **Typewriter** - you can buy them from second-hand shops or markets (I bought a Brother _750 Deluxe_ in Paekākāriki for NZ$35, and an Olivetti _Valentine_ for NZ$90 in Mayfield). Depending on your location, you may be able to hire one. For Mozilla Festival, I hired an East German _Erika_ from [London Typewriters](http://www.londontypewriters.co.uk/). (For this typewriter, the hire price was £40, or it was £105 to buy).

![Why we use a typewriter](Images/README-Images/typewriters.jpg)

A typewriter may seem like a silly novelty or some kind of just-for-show hipster accessory, but it's actually an extremely functional tool for this workshop, as it allows you to type standardised, easily legible type _directly onto_ an existing work. The fact that it is really fun is a pleasant side effect :)

- **Typography** for titles, graphics etc. [Letraset](https://en.wikipedia.org/wiki/Letraset) letters are probably overkill, but you could likely make nice hedings by printing out [these alphabet sheets](Print/Typography/) in different typefaces and weights, and allowing participants to cut and glue them into place as needed. 

- **Public Domain / Free Culture imagery** - to provide participants with a wide variety of ready-made, ready-to use art, photography, and illustrations which they can work into new forms. You can get started with these [Noun Project illustrations](Print/Noun-Project-Illustrations/).  