_Cut, Copy, Paste is a workshop format to teach people how open source or [peer production](https://en.wikipedia.org/wiki/Peer_production) works. What makes it different is the creative, hands-on approach: you can focus on the concepts and the experience - no digital tools required! Read more about it in the project's [README file](https://gitlab.com/cameralibre/cut-copy-and-paste/blob/master/README.md)_

----
In this document, the upcoming development plan of the _Cut, Copy & Paste_ workshops will be laid out, with all the different tasks to be completed, big and small.  

If you would like to help out with the project in any way, if you have any suggestions, please get in touch:

- [my website](http://www.cameralibre.cc/)  
- [twitter](https://twitter.com/cameralibre)  
- on the [Open Source Design](https://discourse.opensourcedesign.net/t/teaching-open-source-collaboration-to-designers-without-digital-tools/289) forum
- or open an [issue](https://docs.gitlab.com/ee/user/project/issues/index.html) here on Gitlab. If you go to this project's [Issues](https://gitlab.com/cameralibre/cut-copy-and-paste/issues) page, you will see the option to simply email an issue. Follow the instructions and if you get stuck, let me know :)

# ROADMAP 

## Upcoming workshops

### Fellows Summit - Mozilla Office, San Francisco

Feb 2019 - (not yet confirmed)

Work with my fellow fellows on communicating issues related to their projects.

### Creative Commons Global Summit - Lisbon, Portugal

May 2019

Focused on a topic related to upcoming advocacy campaigns in the CC network.

## Project Development
	
### ⚫ Noun Project API integration ▲
See issue #21:

- a laptop is set up near the printer
- participants search for any term, and the top 8 hits are printed out on A4 paper with attribution and id number 
- id numbers are recorded during committing (ideas still needed to improve this aspect)
- when the participant is putting together their zine, they can make a list of id numbers used. When typed into the laptop, a credits list is printed, eg: 

![np-credits-paths.svg](/uploads/d46f6c49847b49d6c23c8c162b706eab/np-credits-paths.svg)

- This can then be added to the last page of the zine.

### 💻 PROJECT WEBSITE 🏗
Set up a website succinctly communicating this project, and reformat the existing recipes into different modules which can be combined or adapted by others. This will likely be a simple Gitlab Pages static site for now.

### 🗣 PROJECT COMMUNICATION 📣
- video GIFs of each stage in the process
- record audio of 5min Lightning Talk & cut it into a video with the slides.

### 🕰 PUBLIC DOMAIN REPOSITORY 📚
Create a digital repository of public domain imagery which can be printed out and built upon for each workshop. Involves searching, formatting for A4 B+W printing, and organising/tagging. 


## Developments on hold (for now)

### ✏ ANIMATION WORKSHOP 🎞
Develop a Cut, Copy & Paste workshop for making simple 2D black & white animations using the photocopier. Due to the technical complexity of animation as a medium, this would first be aimed at animators, motion graphics artists, and illustrators.

Each A4 page is cut into 12× frames, making a 1-second animation @ 12fps. 
![](Animation-Workshop/animation-layout.svg)

#### Example animations
Make a series of example animations to test out the process and ultimately communicate the workshop itself. 

#### Develop 'Gameplay'
- Work out how co-creation of animations should occur
- come up with games/activities for getting participants started
- come up with methods to avoid bottlenecks

#### Bash script 1: scanned A4 -> video
_(see #17 for current status of development)_

Write a script using ffmpeg which cuts a 600dpi scan of an animation into a 12-frame image sequence, and makes a video file from it (necessary to allow participants to immediately review their work). 
User input should allow for: 

**0:** a 1-second video, just the 12 frames of animation.

**1:** a 2-second video, comprising a 1-sec preroll of the first frame, then the 12-frame animation.

**2:** a 2-second video, comprising the 12-frame animation, then a 1-sec hold of the last frame, 

**3:** a 3-second video, comprising a 1-sec preroll of the first frame, then the 12-frame animation, then a 1-sec hold of the last frame.

The script should then play the video. 

#### Bash script 2: Backgrounds
_(see #17 for current status of development)_

Write a script using imagemagick which: 

- takes a scanned A4 page containing a background illustration 

- crops it to the correct aspect ratio

- scales down & arranges that image 12x as 12 frames on a new A4 page

- prints the image



## Completed Roadmap tasks
### ~~README~~
- ~~Write more about the WHY of the project: what is the vision, how does this project get us further towards that vision? (currently [working on this](https://gitlab.com/cameralibre/cut-copy-and-paste/blob/master/WHAT-IS-THE-POINT.md))~~
- ~~briefly summarize what happens in a workshop - the commit process, forking etc~~

### ~~[Buy/make/rent/borrow necessary equipment for workshops](https://gitlab.com/cameralibre/cut-copy-and-paste/issues/3)~~

- ~~Creative Commons rubber stamps (BY-SA, BY & CC0)~~ - I've ordered some
- ~~Alphabet stamp sets for people to make their own name stamps~~ - ordered, arrived.
- ~~inkpads for rubber stamps, blue & red~~ - bought already
- ~~[Automatic numbering stamp](https://en.wikipedia.org/wiki/Bates_numbering) for assigning commit numbers~~
- ~~paper guillotine~~(a craft knife, ruler and cutting board is more transportable and simpler)
- ~~design & print a 'commit log' on A4(?) sheets of paper~~
- ~~accordian-style [expanding file](https://duckduckgo.com/?q=accordian+folder&t=ffab&iax=1&ia=images) to serve as the repository~~
- ~~stickers for covering old commit numbers~~
- ~~magnets for attaching work to the help-needed board?~~ (use a table instead of a board)
- ~~2x typewriters - one will be fine for each workshop - but this should be QWERTZ in Germany & QWERTY in the UK if possible~~ (bought one)
- ~~photocopier - I'm hiring one for MozFest. Unsure about OSD Summit yet.~~ 

### ~~🍕 Pizza Thursday 🍕~~
~~I'm doing a 20min presentation of Cut, Copy & Paste at [Catalyst](https://catalyst.net.nz/)'s regular lunchtime show-and-tell on April 5th.~~

### ~~🍁 Cut, Copy & Paste goes to Toronto! 🏒~~
~~I'll be doing a short Cut, Copy & Paste workshop at the Creative Commons Global Sprint, April 13th-15th in Toronto.~~
~~The session space is shared with Matt Lee, whose workshop is about making free culture using free software, so I'll be developing a version of the workshop to fit alongside that topic.~~

### ~~WORKSHOP @ Open Source Design Summit, 13-17th Oct 2017~~

~~[Teaching Open Source collaboration to designers, without digital tools](https://discourse.opensourcedesign.net/t/teaching-open-source-collaboration-to-designers-without-digital-tools/289)~~

~~Workshopping the workshop: running a short prototype of the photocopy workshop to create a zine or two on a set topic. Participants will be designers who work on open source software projects - so they are already familiar with version control, distributed collaboration etc.~~
~~Therefore, this will be a somewhat meta-workshop, where we talk about the issue of getting designers interested in open source processes, the hurdles we face, and the participants can provide feedback on the workshop format and the analog processes we use.~~

### ~~BLOGPOST & RECIPE, after Open Source Design Summit~~

### ~~WORKSHOP @ MozFest 27-29th, Oct 2017~~
~~[Cut, Copy & Paste: Use (analog!) open source collaboration to make your own MozFest Zine](https://github.com/MozillaFoundation/mozfest-program-2017/issues/472)~~

~~Here I assume that not all of the participants will have a deep understanding or much experience of distributed open source collaboration (though many will, of course!).~~
~~This workshop will be more open-ended in terms of content than the OSD Summit - participants can work on any topic they like, and although feedback on the workshop is appreciated, it is not the explicit focus - instead, we'll be genuinely trying to document diverse aspects of MozFest & give participants an understanding of Open Source collaboration~~

### ~~BLOGPOST & RECIPE, after MozFest~~


