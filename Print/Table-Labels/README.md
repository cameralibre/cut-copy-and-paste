These labels can be printed, cut in half and attached to each particular workstation to help orient participants within the workshop:
![](../../Images/README-Images/mozhouse-18_file-table.jpg)
![](../../Images/README-Images/mozhouse-18_remix-table.jpg)
