# The Noun Project

One way that you can start a workshop is by showing people that there is a vast existing commons out there _which they can already use_ in their work.
 
One example of this is [The Noun Project](https://thenounproject.com/) where you can search for pretty much anything, and find icons and illustrations available under Creative Commons Attribution, downloadable as SVG or PNG. 
Here I have laid out a number of images loosely gathered under the broad umbrella of 'technology', which are in a variety of styles (I chose many 'hand-drawn' style images to suit the zine aesthetic). 

![](../../Images/README-Images/skis.png) ![](../../Images/README-Images/ruler.png)

In this folder you'll find a number of printable A4 PDFs. In the SVG folder you'll find the multi-layered SVG file used to arrange them, and the source images as downloaded from the Noun Project, all licensed CC-BY.

I've since developed a browser-based interface which can help you find, use and attribute Noun Project images: 


![Noun Project Images: Printing from the API](./Print-from-API/Noun-Project_print-from-API.webm)

![Noun Project Search Page](../../Images/README-Images/np-search-print-output.png)

You can print and cut these A4 sheets into separate images (a paper cutter/guillotine helps to do this quickly). The images can be arranged on the remix table before the workshop starts, and you might set a first task such as: 
> _"choose one or two images from the collection, and use them to create the title page for an imaginary zine. Write a Title and a Tagline to go with the imagery and communicate what the zine is about. This is just an imagination exercise for now - it doesn't have to be the zine that you will work on in this workshop, but instead it should be a zine that you would like to read - the zine ideas can be as complex, unusual or as ridiculous as you like. You can cut the images up, you can draw in anything else you like."_


Each Noun Project file has a unique number attached to it, which we can write into the 'Builds Upon' column of the Commit Log. If you search for that id on the Noun Project, you'll find the icon and its author info.
![search results - one icon found](../../Images/README-Images/noun-project-id-found.png)

But it is much, much easier to use the credits page that I've set up - see above video or the [Print-from-API](./Print-from-API) folder.

![Noun Project Zine Credits Page](../../Images/README-Images/np-credits-print-output.png)