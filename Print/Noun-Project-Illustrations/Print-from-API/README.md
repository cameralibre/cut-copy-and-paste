# Using the Noun Project API to make use and attribution of images easier
(see Issue #21, [_'Making accurate credits takes a long time'_](https://gitlab.com/cameralibre/cut-copy-and-paste/issues/21) for more context)

![Noun Project Images: Printing from the API](Noun-Project_print-from-API.webm)

This is a very simple search interface for the Noun Project, designed to be set up on a laptop at the workshop. It uses the Noun Project API and a proxy server on the same computer for authentication.

In the search interface, a workshop participant can type in a word, eg. _banana_, and the top 8 images returned will be immediately printed out, with license, attribution, ID, and a thumbnail (they will likely cut the metadata off the image, so the thumbnail helps them link it back to the image they have used)

### Current layout:

![](../../../Images/README-Images/np-search-print-output.png)

I would like to prioritize Noun Project images with the 'handdrawn' tag but I haven't worked out how to do that yet.

There's also an automated credits page - participants can note down the images they have used (by referring to the commit log, or by gluing the 'metadata' info to the back of their work). They then type a series of IDs into the connected laptop. Up to 8 IDs can be input, and a credits page is printed.
(for now, if a participant uses more than 8 images, they can print multiple credits pages and hack them together with scissors and glue)

### Current layout:

![](../../../Images/README-Images/np-credits-print-output.png)

The layouts are printing nicely and have been tested in a workshop at the Mozilla Fellows Winter Summit in San Francisco.

Many thanks to [Darius Kazemi](https://tinysubversions.com/) for helping me with OAuth authentication and setting up the proxy server!

### Using the interface
To run this yourself, you'll need a free 'Playground' access key from the Noun Project's [Developers page](https://thenounproject.com/developers/). 

Download this folder! You will need to paste your Noun Project key and secret into lines 47 & 48 of index.js for it to authenticate properly.

You'll also need to install [node.js](https://nodejs.org/en/) for your system.


 In your terminal, [change directory](https://tutorial.djangogirls.org/en/
intro_to_command_line) to this folder, and type 

```
npm install

node index.js
``` 
to start the server.

If it's working, you should see a message saying ```Example app running on port 3000!```

If you have any trouble, make sure that the ```cors``` module is installed. Still in the terminal, type ```npm install cors``` to install it.

Once the server is running, you can open ```search-results.html``` with a browser. Although Firefox is my go-to browser, but I found that printing from [Beaker Browser](https://beakerbrowser.com) gave me more reliable results with smaller margins - I assume any Chromium-based browser will perform similarly.