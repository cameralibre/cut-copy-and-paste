var crypto = require('crypto');
var OAuth = require('oauth-request');

const express = require('express')
const app = express()
const cors = require('cors');
const port = 3000

app.get('/', cors(), (req, res) => {
  console.log(req.query);
  let term = req.query.term;
  getImages(term).then((data) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.json(data)
  });
});

app.get('/icon', cors(), (req, res) => {
  console.log(req.query);
  let icon = req.query.icon;
  getIcon(icon).then((data) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.json(data)
  });
});

app.get('/user', cors(), (req, res) => {
  console.log(req.query);
  let username = req.query.username;
  let page = req.query.page;
  getUserUploads(username, page).then((data) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.json(data)
  });
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))


function getImages(term) {
  return new Promise((resolve, reject) => {
    var nounProject = OAuth({
        consumer: {
           key: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
           secret: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
// ENTER YOUR NOUN PROJECT KEY & SECRET HERE
        },
        signature_method: 'HMAC-SHA1',
        hash_function: function(base_string, key) {
            return crypto.createHmac('sha1', key).update(base_string).digest('base64');
        }
    });


    nounProject.get({
        // url: 'https://api.thenounproject.com/icons/' + term
        url: 'https://api.thenounproject.com/icons/' + term,
        qs: {
            limit: 8
        },
        json: true
    }, function(err, res, icons) {
        resolve(icons);
    });
  });
}

function getIcon(icon) {
  return new Promise((resolve, reject) => {
    var nounProject = OAuth({
        consumer: {
           key: 'b55cb0b73b784d579415f0e668d3b487',
           secret: 'e0fc5d67199f44d7a23b6fc4337aea44'
// ENTER YOUR NOUN PROJECT KEY & SECRET HERE
        },
        signature_method: 'HMAC-SHA1',
        hash_function: function(base_string, key) {
            return crypto.createHmac('sha1', key).update(base_string).digest('base64');
        }
    });


    nounProject.get({
        // url: 'https://api.thenounproject.com/icons/' + term
        url: 'https://api.thenounproject.com/icon/' + icon,
        json: true
    }, function(err, res, icon) {
        resolve(icon);
    });
  });
}


function getUserUploads(username, page) {
  return new Promise((resolve, reject) => {
    var nounProject = OAuth({
        consumer: {
           key: 'b55cb0b73b784d579415f0e668d3b487',
           secret: 'e0fc5d67199f44d7a23b6fc4337aea44'
// ENTER YOUR NOUN PROJECT KEY & SECRET HERE
        },
        signature_method: 'HMAC-SHA1',
        hash_function: function(base_string, key) {
            return crypto.createHmac('sha1', key).update(base_string).digest('base64');
        }
    });


    nounProject.get({
        // url: 'https://api.thenounproject.com/icons/' + term
        url: 'https://api.thenounproject.com/user/' + username + "/uploads?limit=8&page=" + page,
        json: true
    }, function(err, res, icon) {
        resolve(icon);
    });
  });
}
