function getUserIcons() {
  let term = document.getElementById('text').value;
  let page = document.getElementById('page').value;
fetch('http://localhost:3000/user/?username=' + term + "&limit=8&page=" + page)
  .then(function(response) {
    return response.json();
  })
  .then(function(userData) {
    console.log(userData);

    var icons = userData.uploads

    for(i = 0; i < 8; i++){

      var id = icons[i].id
      var pngImage = icons[i].attribution_preview_url
      var svgImage = icons[i].icon_url
      var license = icons[i].license_description

      document.getElementById(( "id-" + i )).textContent = id

      if (license === "creative-commons-attribution"){
      document.getElementById(( "license-" + i )).setAttribute("href", "#cc-by")
      document.getElementById(( "image-" + i )).setAttribute("href", pngImage)
      }
      else {
      document.getElementById(( "license-" + i )).setAttribute("href", "#cc0")
      document.getElementById(( "image-" + i )).setAttribute("href", svgImage)
     }
    }

});
}
