function getTheData() {
  let term = document.getElementById('text').value;
fetch('http://localhost:3000/?term=' + term)
  .then(function(response) {
    return response.json();
  })
  .then(function(iconData) {
    console.log(iconData);

    var icons = iconData.icons

    for(i = 0; i < 8; i++){

      var id = icons[i].id
      var pngImage = icons[i].attribution_preview_url
      var svgImage = icons[i].icon_url
      var license = icons[i].license_description

      document.getElementById(( "id-" + i )).textContent = id

      if (license === "creative-commons-attribution"){
      document.getElementById(( "license-" + i )).setAttribute("href", "#cc-by")
      document.getElementById(( "image-" + i )).setAttribute("href", pngImage)
      }
      else {
      document.getElementById(( "license-" + i )).setAttribute("href", "#cc0")
      document.getElementById(( "image-" + i )).setAttribute("href", svgImage)
     }
    }

});
}



// button.addEventListener("click", printPage)
//
// function printPage(e) {
//  window.print()
//
// }
