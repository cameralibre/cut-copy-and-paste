# Zines

Three printable templates for making 8- or 16-page zines from an A4 sheet of paper, plus a zine about Cut, Copy & Paste which can be adapted, printed and handed out to participants.

## Zine Templates
These will help your participants to lay out their own zines on a single A4 sheet of paper.

Probably the easiest (and therefore best) choice for a workshop is using the Folding 8-page zine:

![folding-zine-template](../../Images/README-Images/folding-zine.png)

Here's a great tutorial that shows how to put this zine together: [How to make a one-page zine](http://experimentwithnature.com/03-found/experiment-with-paper-how-to-make-a-one-page-zine/)

## Cut, Copy & Paste Zine

![](../../Images/README-Images/zine-cover.jpg)![](../../Images/README-Images/zine-pages.jpg)

If you're running a Cut, Copy & Paste workshop, feel free to adapt the zine to your own purposes - you may want to update the information, refer to different projects, or make some reference to the context or topic of your workshop. 

The Cut, Copy and Paste zine is in A7 format (A4 cut into 4, then folded)

It uses the '16-page stapled book' template (printed double-sided on white paper), with an additional cover page (printed on red paper), making it 20 sides in total.
 
In order to print 40x zines, I would print:

- 40x ```CCP-zine_BW_2-SIDED_A4_PRINT-ON-WHITE.pdf```  
- 10x ```CCP-zine-cover_BW_1-SIDED_A4_PRINT-ON-RED.pdf``` 
- before cutting, compiling, folding and stapling.

If you would like to adapt the zine, don't forget to change the name and contact details on the back cover to your own details:

1. Open ```CCP-zine-cover.svg``` in [Inkscape](https://inkscape.org/)
2. In the Layers panel, turn on visibility 👁️ of the layer '**YOUR CONTACT DETAILS**'. 
3. Replace the following with your contact details (in each of the 4 panes)
: 

>     Your Name
>     you@email.address         @git-twit-id


In CCP-zine.svg, make sure you change the **About This Zine** page to read:


>     This publication is © [YOUR NAME], 
>     released under the Creative Commons Attribution ShareAlike license: 
>     creativecommons.org/licenses/by-sa/4.0
>   
>     This work builds upon "Cut, Copy & Paste Zine", CC-BY-SA Sam Muirhead.

It's not a legal requirement, but if you make a new version of the zine, please share it here! 




