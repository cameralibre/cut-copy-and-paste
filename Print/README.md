Assorted printable PDFs which will make the job of facilitating a Cut, Copy & Paste workshop easier! Editable SVGs are also available should you need to customise anything. 
You'll find a more detailed README in each folder:
 
## [Commit Log](Commit-Log)

The form which participants use to log each work before it is photocopied.

![Commit Log](../Images/README-Images/Commit-Log.png)


## [Cut, Copy & Paste Posters](Cut,-Copy-&-Paste-Posters)

Print and post these in the room where you're running the workshop (so that they know the name of the workshop and where to find this repository). You can also post these on the door of the property/building etc, with an arrow to direct participants to the right place.

![Cut, Copy & Paste 'Hello World' Poster](../Images/README-Images/CCP-hello-world-poster.png)


## [Instructional Posters](Instructional-Posters)

Explanatory posters of different elements in the workshop, so that participants are able to learn on their own or teach others, without relying on the facilitator as a single point of information.

![How To Commit - Logging](../Images/README-Images/How-To-Commit_1-Logging.png)

## [Noun Project Illustrations](Noun-Project-Illustrations)

Over 400 images from [The Noun Project](https://thenounproject.com/) which can be used and remixed in a workshop, plus a custom software interface which can be used to print more images, or automatically create credits pages:

![Noun Project Images: Printing from the API](Noun-Project-Illustrations/Print-from-API/Noun-Project_print-from-API.webm)

![](../Images/README-Images/stationery.png)

## [Table Labels](Table-Labels)

These labels can be attached to each particular workstation to help orient participants within the workshop:
![](../Images/README-Images/mozhouse-18_file-table.jpg)

## [Typography](Typography)

A small collection of Libre typefaces as printable PDFs - participants can cut and paste letters for titling etc.
![](../Images/README-Images/serreria-letters.png)

## [Zines](Zines)

Two printable templates for making 8- or 16-page zines, plus zines about Cut, Copy & Paste which can be printed and handed out to participants.

![](../Images/README-Images/zine-cover.jpg)![](../Images/README-Images/zine-pages.jpg)



