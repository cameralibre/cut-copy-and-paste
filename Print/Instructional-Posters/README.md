## Instructional Posters

This folder contains explanatory posters of different elements in the workshop, so that participants are able to learn on their own or teach others, without relying on the facilitator as a single point of information.

Printable PDFs and editable SVGs are provided. 


- **CC-BY-SA-WTF** - an explanation of CC-BY-SA and open source licensing in general:

![CC-BY-SA-WTF](../../Images/README-Images/CC-BY-SA-WTF.png)

- **Commit Example** (A3) - showing an original work, its remix, and the commit being entered:

![Commit Example](../../Images/README-Images/Commit-Example.png)


- **How-To-Commit_1-Logging** - a step-by-step outline of the workflow at the commit table:

![How To Commit - Logging](../../Images/README-Images/How-To-Commit_1-Logging.png)


- **How-To-Commit_2-Filing** -  showing how to copy, file and display works:

![How To Commit - Filing](../../Images/README-Images/How-To-Commit_2-Filing.png)

- **How Can Work Be Taken Further?** - ideas for approaches that participants could take to remix, adapt or improve a work. (Poster orignally created by Judith Carnaby)

![How Can Work Be Taken Further?](../../Images/README-Images/How-Can-Work-Be-Taken-Further.png)
