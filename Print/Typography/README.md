# Typography

A small collection of Libre typefaces as printable PDFs - participants can cut and paste letters for titling etc.

![](../../Images/README-Images/serreria-letters.png)

(You might also consider using [dry transfer](https://en.wikipedia.org/wiki/Dry_transfer) sheets for this purpose - they are more expensive, but they will work a lot better)