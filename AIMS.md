the TL;DR:
### Problem

-   The way we create/ think about artistic work can devalue or impede remix and 'derivative' works, focusing instead on individual brilliance and ownership.
-   Creative collaboration is generally restricted to a small group of known participants in the same place.
-   Open source is often seen as just for software, not culture.
-   It’s hard to see the value of open source without directly experiencing it for yourself.

### Solution

- Give artists a fun, tangible & inclusive experience of open source collaboration, to get them thinking & working in this way.
- Develop a curriculum of hands-on creative workshops which can be used to teach different aspects of the open source process, and prepare participants to move into digital collaboration.

----


# AIMS of CUT, COPY & PASTE

I have been [advocating for open source beyond software](http://www.cameralibre.cc/sourcecode-berlin) for years, but I often feel that just telling people isn’t enough. In theory, open source culture, hardware, and [animation](https://gitlab.com/cameralibre/FragDenStaat-Animation) sounds nice, but to many it sounds… well, like a theory.

The people who really [grok](https://en.wikipedia.org/wiki/Grok) open source are those who have experienced it. Programmers learn open source concepts from the moment they’re learning to code. They can inspect other people’s work, view the source of a web page, use open source learning resources, build upon open source libraries, etc.

For most artists & designers, the idea of publishing unfinished work, or basing your work upon somebody else’s is scary enough. When you’re also giving up the copyright monopoly that allegedly is the sole way to earn a living… that’s against everything we learn at art/media/design school, and all the advice we receive from the creative industry.

Even if somebody excitedly tells you about this other way to do things – no matter how interesting it may sound – beyond software, open source is still seriously niche, and it requires a profound change in your practice and your business model. That’s too risky, too much work, and the likelihood of actually getting the purported benefits of open source collaboration is low, because so few people are doing it.

So in order to kick-start more open source collaboration outside of software, Cut, Copy & Paste is an attempt to give non-coders an open source experience in a no-risk, low-tech environment, with a very high likelihood of work being remixed. Here the focus is on the concepts and the potential of this process, rather than learning git commands.

In the long term, I hope to develop a curriculum of hands-on creative workshops which can be used to teach different aspects of the open source process. Each workshop should be adaptable to a different topics/themes (eg. for OSD Summit we were discussing bringing designers towards adopting OS methods, and at MozFest we were sharing individual experiences of Mozfest) 

So far, I have developed & tested two workshops:

- a [public domain collage workshop](http://cameralibre.cc/arts-commons), which aims to emphasise the value of the commons, the practical use of documentation, and to change people's understanding of 'authorship'.
- a [zine-making workshop using analog version control](http://cameralibre.cc/cut-copy-and-paste), which gives people an experience of remixing and being remixed, of forking, and of curating an individual canon from a broader commons.

Loose drafts are being developed for:

- a comics jam that teaches open leadership, pull requests, and interpersonal processes for collaboration.
- some kind of text-based collaborative poetry game would also be a nice approach (#6 ).

These analog workshops could perhaps be followed by a series of **digital** open source creative workshops, (maybe with [git-physical](https://github.com/MMinkova/git-physical) as a bridge?) or a program like [Mozilla Open Leadership](https://mozilla.github.io/leadership-training/) - some way to gradually transition participants from the 'fun, analog learning game' experience to something more directly relevant and tied in to their [creative] work.