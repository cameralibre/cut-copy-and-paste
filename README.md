![](./Images/README-Images/CCP-hello-world-poster.png)
# Cut, Copy & Paste
_Get to grips with the creative and surprising world of online collaboration... in an analog, offline workshop! Make and remix zines, and see the potential for limitless artistic creation that 'forking' and remixing provides. All using tools no more complicated than scissors, rubber stamps and a photocopier._ 

----

![](./Images/README-Images/typewriter-CC-BY-NC_Eric-Westra.jpg)
_CC-BY-NC [Eric Westra](https://www.flickr.com/photos/mozfest/albums/72157703025472325)_

## Contents

- [**What is this thing**](#what)
- [**More Details**](#more)
- [**What's the Point?**](#why)
- [**An Example**](#example)
- [**Can I use this?**](#use) _(spoiler: **yes**)_
- [**I want to take part**](#take-part)
- [**I have an idea**](#idea)
- [**Who is involved?**](#team)
- [**Other tools for learning version control**](#tools)


### What is this thing? <a name="what">
"Cut, Copy & Paste" is a workshop format to teach people how commons-based collaboration and remix works. What makes it different is the creative, hands-on approach: you can focus on the concepts and the experience - no digital tools required! 

Participants write down stories, draw pictures, cut up and collage existing material, and build up a commons together, forking and remixing each others' ideas. They can then draw on this commons to make their own unique, self-published 'zine' - a small booklet that tells a story in an accessible, fun and quirky way. The zine is an expression of their own subjective perspective, taste and ideas, but it is made possible through collaboration with others.

Cut, Copy & Paste was originally developed as part of the [Mozilla Open Leaders](https://mozilla.github.io/leadership-training/) program, and prototyped at Open Source Design Summit, Mozilla Festival, the Creative Commons Global Summit, and Wellington ZineFest.

![commit log being stamped](Images/README-Images/stamping.jpg)

### I want more details <a name="more">
Sure thing:

- The workshop uses stamps, a photocopier and a paper 'commit log' as a '[version control](https://en.wikipedia.org/wiki/Version_control)' method to store copies of every step in the creative process - here's an (illustrated) explanation of [how the core process of creating, committing and remixing works](How-to-commit.md). 
- the [Equipment List](Equipment_list.md) has everything you will need to run a workshop.
- the instructional [Posters](Print/Instructional-Posters/) and [Commit Log](Print/Commit-Log/), [zine templates](Print/Zines), [illustrations](Print/Noun-Project-Illustrations) and more [resources to print](Print) or adapt.
- a [blogpost](http://www.cameralibre.cc/cut-copy-and-paste/) about the workshop we ran at Mozilla Festival 2017
- a [blogpost](http://www.cameralibre.cc/arts-commons/) and [recipe](https://gitlab.com/cameralibre/arts-commons/blob/master/Co-creation%20Collage%20Workshop.md) for a more basic version of the workshop at Arts & Commons in 2016
- The [Roadmap](https://gitlab.com/cameralibre/cut-copy-and-paste/blob/master/ROADMAP.md) outlining the upcoming tasks and milestones for the project.

![](./Images/README-Images/copies-CC-BY-NC_Eric-Westra.jpg)
_CC-BY-NC [Eric Westra](https://www.flickr.com/photos/mozfest/albums/72157703025472325)_

![Workshop Setup](Images/README-Images/setup.jpg)

### Yeah, but what's the point? <a name="why">
See the [vision document](AIMS.md) to understand more about the aims of this project. 
_Cut, Copy & Paste_ is a companion project to my main focus, [Digital Co-Creation](https://vimeo.com/sammuirhead/drawing-on-the-commons) and [Open Source Animation](https://gitlab.com/cameralibre/FragDenStaat-Animation). 
If I want open source animation (and other types of artistic peer production) to succeed, then I need to inspire artists to try working in this way. That's where these workshops come in.

### An example / warm-up exercise: <a name="example">

![evolution by unnatural selection](Images/README-Images/evolution-by-unnatural-selection_01.jpg)
![evolution by unnatural selection continued](Images/README-Images/evolution-by-unnatural-selection_02.jpg)

### Can I use this? <a name="use">

Of course - if you would like to run a version of these workshops, go ahead - you don't need my permission! But I'm very happy to help you adapt them, or to look for solutions to any problems you may come across. I'd love to hear what you do with it!

I am based in Aotearoa New Zealand, so I'd love other people to pick up the project and run workshops in places which are harder for me to get to (so... pretty much anywhere).

But if you publish a new version, you need to attribute me and use the same license that I have used. (Everything in this repository (unless otherwise indicated) is licensed under [Creative Commons Attribution-Sharealike](Print/Instructional-Posters/CC-BY-SA-WTF.pdf).)

### I want take part in a workshop! <a name="take-part">

If you would like me (or someone else) to run a workshop for you or your organisation, please get in touch, I can help you to find the right kind of workshop and the best person/people to facilitate it.

### I have an idea! / Check out this resource! <a name="idea">
Have you seen other interesting approaches to teaching open source to artists? How would you communicate the open source approach in a fun, lo-tech way? Can you solve any of these [Issues](https://gitlab.com/cameralibre/cut-copy-and-paste/issues)?

If you have any suggestions or improvements please get in touch:
 
- [my website](http://www.cameralibre.cc/)  
- [twitter](https://twitter.com/cameralibre)  
- Secure Scuttlebutt: @UhORGzAhEE3gqy/pH5vK+EgbpZfnyYvdI46TVBJH6Mw=.ed25519
- on the [Open Source Design](https://discourse.opensourcedesign.net/t/teaching-open-source-collaboration-to-designers-without-digital-tools/289) forum
- or open an [issue](https://docs.gitlab.com/ee/user/project/issues/index.html) here on Gitlab. If you go to this project's [Issues](https://gitlab.com/cameralibre/cut-copy-and-paste/issues) page, you will see the option to simply email an issue. Follow the instructions and if you need any help with Git or Gitlab, just ask - a lot of it is over my head too, we can learn together :)


### People: Who's involved? <a name="team">

I'm [Sam](https://twitter.com/cameralibre), I'm a Mozilla Fellow working with Creative Commons, based in Te Whanganui-a-Tara (Wellington), Aotearoa New Zealand. I like to explore, explain and advocate for Open Source beyond the world of software. I've often done so by making [documentaries and animations](http://cameralibre.cc): about open everything, made with free software, and built on free culture. 5 years ago I also lived a [Year of Open Source](http://edition.cnn.com/2013/06/04/opinion/sam-muirhead-year-open-source), I've run workshops on community building, licensing and open source hardware business models and co-founded an international movement for an open source approach to create a waste-free, sustainable circular economy ([OSCEdays](https://oscedays.org/)). 

[Judith Carnaby](http://www.judithcarnaby.com/) is an illustrator, teacher and writer on illustration. She has made great visual and conceptual contributions to the development of the project, and will be co-hosting workshops with me.

[Jan-Christoph Borchardt](https://jancborchardt.net/) took part in the first Cut, Copy & Paste workshop prototype in 2017, has contributed many new ideas and great feedback, and is the first person to run a Cut, Copy & Paste workshop without me :)

[Darius Kazemi](https://tinysubversions.com/) helped me get the Noun Project '[Print from API](https://gitlab.com/cameralibre/cut-copy-and-paste/tree/master/Print/Noun-Project-Illustrations/Print-from-API)' interface up-and-running.

[Mercè Rua](https://twitter.com/mruaf) contributed many of the initial ideas of this project. Her continued advice and experience in workshop facilitation and [collaborative practices](http://www.holon.cat/about/) are invaluable!

[David Ross](https://twitter.com/sw1ayfe) mentored me for the Mozilla Open Leadership Program, asking the tricky questions and providing advice and support.

[Abigail Cabunoc Mayes](http://www.abigailcabunoc.com/) & [Monica Granados](https://twitter.com/Monsauce) facilitated my cohort during Mozilla Open Leadership, setting up materials and schedules, and doing all the hard work required to tend for a nursery of little open source seedlings like this one :)

### Other nice tools to learn/teach version control <a name="tools">

##### [Git for Ages 4 & Up](http://www.youtube.com/watch?v=1ffBJ4sVUb4) (video)

##### [Type:Bits](http://typebits.gitlab.io/)
Awesome collaborative type design workshop from [Manufactura Independente](http://manufacturaindependente.org/) - participants design 'bitmap' typefaces by scribbling in squares on grid paper, before digitizing the letters and using Gitlab for collaboration and automated font creation.

##### [git-physical](https://github.com/MMinkova/git-physical/) 
Miglena Minkova's git-physical is a really cool project with similar aims and methods to Cut,Copy,& Paste, though more focused on specifically teaching the version control tool [git](https://en.wikipedia.org/wiki/Git).  
[Read Miglena's article about it!](https://lil.law.harvard.edu/blog/2017/08/10/git-physical/)  
What differentiates the two projects is that:
 
- git physical uses carbon paper as its duplication device, whereas we use a photocopier
- git-physical has a much better name ;)
- the Cut, Copy & Paste workshop is _also_ heavily inspired by git, but ours is a looser interpretation than Miglena's. 
This is because I don't see git (and sites like Gitlab/Github etc) as the solution to artistic version control, at least not in their current state. For me, and perhaps for other non-programmers, git is too esoteric, too text/code-focused, too technical, too imbued with programmer culture ([git blame](https://www.git-scm.com/docs/git-blame), etc). So... if somebody could quickly whip up a simplified, friendlier git equivalent for visual and artistic work, that would be just lovely. Please? Thanks.
